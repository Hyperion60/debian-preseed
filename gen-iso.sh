#!/bin/sh

ARCH=amd64
VERSION=11.5.0
TYPE=netinst

if [ ! -f preseed.cfg ]; then
    python gen-preseed.py
fi

ISOFILE="debian-${VERSION}-${ARCH}-${TYPE}.iso"

if [ ! -f ${ISOFILE} ]; then
    wget "https://cdimage.debian.org/debian-cd/current/${ARCH}/iso-cd/${ISOFILE}"
fi

WORK_DIR=isofiles
MBR_template=isohdpfx.bin

# Extracting files
7z x -o${WORK_DIR} ${ISOFILE}

# Adding a Preseed File to the Initrd
chmod +w -R ${WORK_DIR}/install.*/
gunzip ${WORK_DIR}/install.*/initrd.gz
echo preseed.cfg | cpio -H newc -o -A -F ${WORK_DIR}/install.*/initrd
gzip ${WORK_DIR}/install.*/initrd
chmod -w -R ${WORK_DIR}/install.*/

# Adding firmwares
wget https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/${VERSION}/firmware.tar.gz
tar -xvf firmware.tar.gz -C ${WORK_DIR}/firmware
rm -rf firmware.tar.gz*

# Regenerating md5sum.txt
cd ${WORK_DIR}
chmod +w md5sum.txt
find -follow -type f ! -name md5sum.txt -print0 | xargs -0 md5sum > md5sum.txt
chmod -w md5sum.txt
cd ..

# Creating a New Bootable ISO Image

# Legacy Bios
# genisoimage -r -J -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -o preseed-${ISOFILE} ${WORK_DIR}


# Extract MBR template file to disk
dd if="${ISOFILE}" bs=1 count=432 of="$MBR_template"

# EFI
xorriso -as mkisofs \
   -r -V "Debian ${VERSION} ${ARCH} sed infra" \
   -o "preseed-${ISOFILE}" \
   -J -J -joliet-long -cache-inodes \
   -isohybrid-mbr "$MBR_template" \
   -b "isolinux/isolinux.bin" \
   -c "isolinux/boot.cat" \
   -boot-load-size 4 -boot-info-table -no-emul-boot \
   -eltorito-alt-boot \
   -e boot/grub/efi.img \
   -no-emul-boot -isohybrid-gpt-basdat -isohybrid-apm-hfsplus \
   ${WORK_DIR}


# Cleanup

chmod -R +rw ${WORK_DIR}
rm -rf ${WORK_DIR} ${ISOFILE} ${MBR_template}
