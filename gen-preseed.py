import os
import re
from pathlib import Path

template_regex = re.compile(r"{{\s([a-z_0-9]+)\s}}")

def handle_template(data):

    m = template_regex.findall(data)
    if m:
        for token in m:
            env_name = "DEBPRESEED_" + token.upper()

            data = data.replace(f"{{{{ {token} }}}}", os.getenv(env_name, ""))

    return data

def conbiner(folder_name="preseed.cfg.d", output_file="preseed.cfg"):
    if not os.path.exists(Path(folder_name)):
        raise Exception(f"Config folder {folder_name} not found")
    files = [Path(folder_name) / f for f in os.listdir(Path(folder_name)) if os.path.isfile(Path(folder_name) / f)]
    files.sort()

    generated = ""

    for f in files:
        with open(f, "r") as f:
            data = f.read()

        data = handle_template(data)

        generated += f"### {str(f.name)}\n\n" + data + "\n"

    with open(output_file, "w+") as f:
        f.write(generated)



def main():
    conbiner()

if __name__ == '__main__':
    main()
